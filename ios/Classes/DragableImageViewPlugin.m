#import "DragableImageViewPlugin.h"
#import "FlutterImageView.h"

@implementation DragableImageViewPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
        methodChannelWithName:@"dragable_image_view"
              binaryMessenger:[registrar messenger]];
  DragableImageViewPlugin* instance = [[DragableImageViewPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
    
    FLTImageViewFactory* imageviewFactory =
        [[FLTImageViewFactory alloc] initWithMessenger:registrar.messenger];
    [registrar registerViewFactory:imageviewFactory withId:@"com.xuetangx.mobile/imageview"];
    
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  } else {
    result(FlutterMethodNotImplemented);
  }
}

@end
