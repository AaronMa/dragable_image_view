import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class CupertiImageView extends StatelessWidget {
  final double width;
  final double height;
  final String src;
  final int backgroundColor;
  final String? placeholder;
  final BoxFit fit;
  const CupertiImageView(
      {Key? key,
      required this.width,
      required this.height,
      required this.src,
      this.backgroundColor = 0xFFFFFFFF,
      this.placeholder,
      this.fit = BoxFit.fitWidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var gestureRecognizers = <Factory<OneSequenceGestureRecognizer>>[
      new Factory<OneSequenceGestureRecognizer>(
        () => new EagerGestureRecognizer(),
      ),
    ].toSet();
    print("CupertiImageView src: $src");
    final Map<String, dynamic> creationParams = <String, dynamic>{"src": src};
    return Container(
      width: width,
      height: height,
      color: Color(backgroundColor),
      child: UiKitView(
        key: ValueKey(src),
        viewType: 'com.xuetangx.mobile/imageview',
        gestureRecognizers: gestureRecognizers,
        creationParams: creationParams,
        creationParamsCodec: const StandardMessageCodec(),
        hitTestBehavior: PlatformViewHitTestBehavior.translucent,
      ),
    );
  }
}
