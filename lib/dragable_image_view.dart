
import 'dart:async';

import 'package:flutter/services.dart';

class DragableImageView {
  static const MethodChannel _channel =
      const MethodChannel('dragable_image_view');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
